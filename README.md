JavaScript Game Developer Assignment


## Build

`npm install`

## Run
`npm run dev`


# Comments

I know this coding style in the project isn't really OOP. I think it's hard to accomplish that 
in pure Javascript. I would rather use another language like Haxe or TypeScript and then transpile that into Javascript.

I guess this style is more influenced by functional programming (I really like clojurescript), but I tried not go too crazy with it. 
I like pure functions since they are easy to test and reason about. 

## Known bugs

When the block lands it bounces.

Blocks can get stuck in the wall if flipped too close to it.



Started project with Pixi Webpack Example
https://github.com/arikanmstf/pixi-webpack