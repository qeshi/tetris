import {checkCollision, isInRange, isOutsideRange, isCollidingWithBlocksInMap} from './physics.js'

test('checks Y collision at x 0 y 304', () => {
  let xPosition = 0
  let yPosition = 304
  expect(checkCollision(board, xPosition, yPosition)).toBe(true)

});

test('checks Y collision at x 0 y 303', () => {
  let xPosition = 0
  let yPosition = 303
  expect(checkCollision(board, xPosition, yPosition)).toBe(false)

});

test('checks X collision at x 160 y 0', () => {
  let xPosition = 160
  let yPosition = 0
  expect(checkCollision(board, xPosition, yPosition)).toBe(true)

});

test('checks X collision at x 159 y 0', () => {
  let xPosition = 159
  let yPosition = 0
  expect(checkCollision(board, xPosition, yPosition)).toBe(false)

});

// RANGE

test('check if number is in range', () => {
  let number = 1, max = 10, min = 0
  expect(isInRange(number, max, min)).toBe(true)
});

test('check if number is not in range', () => {
  let number = 10, max = 10, min = 0
  expect(isInRange(number, max, min)).toBe(false)
});

test('check if number is out of range', () => {
  let number = 10, max = 10, min = 0
  expect(isOutsideRange(number, max, min)).toBe(true)
});

test('check if number is not out of range', () => {
  let number = 9, max = 10, min = 0
  expect(isOutsideRange(number, max, min)).toBe(false)
});

// COLLISION

test('check if colliding with filled tile in map', () => {
  let x = 0, y = 19
  expect(isCollidingWithBlocksInMap(board, x, y)).toBe(true)
});

test('check if colliding with y position outside map', () => {
  let x = 0, y = 20
  expect(isCollidingWithBlocksInMap(board, x, y)).toBe(true)
});

test('check if colliding with x position outside map', () => {
  let x = 10, y = 0
  expect(isCollidingWithBlocksInMap(board, x, y)).toBe(true)
});

test('check if not colliding with empty tile in map', () => {
  let x = 0, y = 18
  expect(isCollidingWithBlocksInMap(board, x, y)).toBe(false)
});

let board = [
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [1,0,0,0,0,0,0,0,0,0],
]