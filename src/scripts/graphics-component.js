import { Container, Sprite, TilingSprite } from 'pixi.js'

let app, id, boardGraphics, blockGraphic

const initGraphics = (application, pixiTextures) => {

  app = application
  id = pixiTextures

  let background = new TilingSprite(
    id['background.png'],
    app.screen.width,
    app.screen.height
  )

  boardGraphics = new Container()

  app.stage.addChild(background)
  app.stage.addChild(boardGraphics)

}

const dropNewBlock = (blockModel) => {

  blockGraphic = new Container()

  blockModel.blocks.forEach((pos) => {
    let pieceGraphic = new Sprite(id[typeToGraphic[blockModel.type]]) //TODO refactor
    blockGraphic.addChild(pieceGraphic)
    pieceGraphic.x = pos.x * 16
    pieceGraphic.y = pos.y * 16
  })

  app.stage.addChild(blockGraphic)
}

const updateGraphics = (blockModel) => {
  blockGraphic.y = blockModel.y
  blockGraphic.x = blockModel.x

  blockModel.blocks.forEach((piece, index) => {

    let pieceGraphic = blockGraphic.getChildAt(index)
    pieceGraphic.x = piece.x * 16
    pieceGraphic.y = piece.y * 16

  })

}

const removeBlockGraphic = () => {
  app.stage.removeChild(blockGraphic)
}

const typeToGraphic = {
  I: 'block_cyan.png',
  J: 'block_blue.png',
  L: 'block_orange.png',
  O: 'block_yellow.png',
  S: 'block_green.png',
  T: 'block_purple.png',
  Z: 'block_red.png',

}

const addBlockGraphicsToBoardGraphics = (board) => {

  boardGraphics.removeChildren()

  board.forEach((row, yIndex) => {
    row.forEach((cell, xIndex) => {

      if (cell !== 0) {

        let pieceGraphic = new Sprite(id[typeToGraphic[cell]])
        boardGraphics.addChild(pieceGraphic)
        pieceGraphic.x = xIndex * 16
        pieceGraphic.y = yIndex * 16

      }

    })
  })

}

export { initGraphics, dropNewBlock, updateGraphics, removeBlockGraphic, addBlockGraphicsToBoardGraphics }