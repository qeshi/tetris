const mapTileSize = 16

const getCollisionsForBlocksInModel = (board, model) => {

  const collisions = model.blocks.map( (piece) => {

      let pieceXPos = model.x + mapPositionToWordPosition(piece.x)
      let pieceYPos = model.y + mapPositionToWordPosition(piece.y)

      let newXPosition = pieceXPos + model.vx
      let newYPosition = pieceYPos + model.vy

      return {
        xCollision : checkCollision(board, newXPosition, pieceYPos),
        yCollision : checkCollision(board, pieceXPos, newYPosition),
      }
    }
  )

  return collisions
}

const checkCollision = (board, x, y) => {

  let mapXPosition = worldPositionToMapPosition(x)
  let mapYPosition = worldPositionToMapPosition(y)

  return isCollidingWithBlocksInMap(board, mapXPosition, mapYPosition)
}

const isOutsideRange = (n, max, min) => {
  return !isInRange(n,max,min)
}
const isInRange = (n, max, min) => {
  return (n >= min && n < max)
}

const isCollidingWithBlocksInMap = (board, x, y) => {

  let yIsOutsideMap = isOutsideRange(y, board.length, 0)
  let xIsOutsideMap = isOutsideRange(x, board[0].length, 0)

  if(yIsOutsideMap || xIsOutsideMap){
    return true
  } else {
    let tile = board[y][x]
    return ! (tile === 0)
  }

}

const worldPositionToMapPosition = (n) =>  { return Math.floor(n / mapTileSize)}
const mapPositionToWordPosition = (n) => { return n * mapTileSize}

// this one mutates board
const addBlockToBoard = (board, blockPhysicsModel) => {

  //TODO make sure the blocks are not outside the board!!
  // hamnar utanfor

  let x = worldPositionToMapPosition(blockPhysicsModel.x)
  let y = worldPositionToMapPosition(blockPhysicsModel.y)

  blockPhysicsModel.blocks.forEach( (piece) => {
    let pieceX = x + piece.x
    let pieceY = y + piece.y
    board[pieceY][pieceX] = blockPhysicsModel.type
  })

  return board
}

/*
const snapToNextYTile = (physicsModel) => {
  return mapPositionToWordPosition(worldPositionToMapPosition(physicsModel.y  + physicsModel.vy))
}
*/
export {
  /* snapToNextYTile, */
  mapPositionToWordPosition,
  getCollisionsForBlocksInModel,
  addBlockToBoard,
  checkCollision,
  worldPositionToMapPosition,
  isInRange,
  isOutsideRange,
  isCollidingWithBlocksInMap}