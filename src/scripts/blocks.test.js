import { rotateBlock } from './blocks'

test('check rotate I block', () => {

  let blockModelIHorizontal = {
    x: 0, y: 0, vy: 1, vx: 0,
    blocks: [{x: 0, y: 0}, {x: 1, y: 0}, {x: 2, y: 0}, {x: 3, y: 0}],
    rotation: 0,
    type: 'I'
  }

  let blockModelIVertical = {
    x: 0, y: 0, vy: 1, vx: 0,
    blocks: [{x: 1, y: 0}, {x: 1, y: 1}, {x: 1, y: 2}, {x: 1, y: 3}],
    rotation: 1,
    type: 'I'
  }

  expect(rotateBlock(blockModelIHorizontal))
    .toEqual(blockModelIVertical)

});