import {
  keyboardCallbacks,
  createNewBlock,
  checkCollisionOnBlock,
  getBlockModelCurrentState,
  getBoardCurrentState
} from './physics-component'
import {
  initGraphics, dropNewBlock, updateGraphics, addBlockGraphicsToBoardGraphics,
  removeBlockGraphic
} from './graphics-component'
import { setupKeyboardCallbacks } from './input-component'
import { Application, loader } from 'pixi.js'

let app, id

const initGame = () => {

  app = new Application({
      width: 160,
      height: 320,
      antialias: true,
      transparent: false,
      resolution: 1
    }
  )

  loader
    .add('static/images/tetris.json')
    .load(setup)

  document.body.appendChild(app.view)

  setupKeyboardCallbacks(keyboardCallbacks)

}

let state

const setup = () => {

  id = loader.resources['static/images/tetris.json'].textures

  initGraphics(app, id)

  state = play

  dropNewBlock(createNewBlock()) // nar spelet startar

  //Start the game loop
  app.ticker.add((delta) => gameLoop(delta))
}

const gameLoop = (delta) => {
  state(delta)
}

const play = (/*delta*/) => {
  checkCollisionOnBlock(handleYCollision)
  updateGraphics(getBlockModelCurrentState())
}

const handleYCollision = () => {
  removeBlockGraphic()
  addBlockGraphicsToBoardGraphics(getBoardCurrentState())
  dropNewBlock(createNewBlock())
}

export { initGame }
