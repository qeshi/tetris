
const getFilledRows = (board) => {

  let filledRows = board.reduce((accumulator, row, index) => {

    if(row.every((cell) => (cell != 0))){
      accumulator.push(index)
    }

    return accumulator
  }, [])

  return filledRows
}

const emptyRow = [0,0,0,0,0,0,0,0,0,0]

const clearFilledRows = (board) => {

  let clearedBoard = board.filter((row) => {
    return !row.every((cell) => (cell != 0))
  })

  const numberOfEmptyRowsToAdd = 20 - clearedBoard.length

  for(let i = 0; i < numberOfEmptyRowsToAdd ;i++ ){
    clearedBoard.unshift(emptyRow)
  }

  return clearedBoard
}

export {getFilledRows, clearFilledRows}