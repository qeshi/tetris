import { merge , sample} from 'lodash'

const blocks = {
  I: {
    rotations: [[{x: 0, y: 0}, {x: 1, y: 0}, {x: 2, y: 0}, {x: 3, y: 0}],
      [{x: 1, y: 0}, {x: 1, y: 1}, {x: 1, y: 2}, {x: 1, y: 3}]]
  },
  J: {
    rotations: [[{x: 0, y: 0}, {x: 0, y: 1}, {x: 1, y: 1}, {x: 2, y: 1}],
      [{x: 0, y: 0}, {x: 0, y: 1}, {x: 0, y: 2}, {x: 1, y: 0}],
      [{x: 0, y: 0}, {x: 1, y: 0}, {x: 2, y: 0}, {x: 2, y: 1}],
      [{x: 1, y: 0}, {x: 1, y: 1}, {x: 1, y: 2}, {x: 0, y: 2}],
    ]
  },
  L: {
    rotations: [[{x: 2, y: 0}, {x: 0, y: 1}, {x: 1, y: 1}, {x: 2, y: 1}],
      [{x: 0, y: 0}, {x: 0, y: 1}, {x: 0, y: 2}, {x: 1, y: 2}],
      [{x: 0, y: 0}, {x: 1, y: 0}, {x: 2, y: 0}, {x: 0, y: 1}],
      [{x: 0, y: 0}, {x: 1, y: 0}, {x: 1, y: 1}, {x: 1, y: 2}],
    ]
  },
  O: {
    rotations: [[{x: 0, y: 0}, {x: 1, y: 0}, {x: 0, y: 1}, {x: 1, y: 1}]]
  },
  S: {
    rotations: [[{x: 1, y: 0}, {x: 2, y: 0}, {x: 0, y: 1}, {x: 1, y: 1}],
      [{x: 0, y: 0}, {x: 0, y: 1}, {x: 1, y: 1}, {x: 1, y: 2}]
    ]
  },
  Z: {
    rotations: [[{x: 0, y: 0}, {x: 1, y: 0}, {x: 1, y: 1}, {x: 2, y: 1}],
      [{x: 1, y: 0}, {x: 0, y: 1}, {x: 1, y: 1}, {x: 0, y: 2}]
    ]
  },
  T: {
    rotations: [[{x: 1, y: 0}, {x: 0, y: 1}, {x: 1, y: 1}, {x: 2, y: 1}],
      [{x: 0, y: 0}, {x: 0, y: 1}, {x: 0, y: 2}, {x: 1, y: 1}],
      [{x: 0, y: 0}, {x: 1, y: 0}, {x: 2, y: 0}, {x: 1, y: 1}],
      [{x: 1, y: 0}, {x: 0, y: 1}, {x: 1, y: 1}, {x: 1, y: 2}],
    ]
  },
}

const rotateBlock = (model) => {

  let rotations = blocks[model.type].rotations

  let rotationIndex
  if ((rotations.length - 1) === model.rotation) {
    rotationIndex = 0
  } else {
    rotationIndex = model.rotation + 1
  }

  return Object.assign(model, {rotation: rotationIndex, blocks: rotations[rotationIndex]})

}

const getNewBlock = () => {

  let type = sample(Object.keys(blocks))
  //let type = "J"
  let pieces = blocks[type].rotations[0]
  let newBlock = merge({ x: 0, y: 0, vy: 1, vx: 0, rotation: 0 }, {type: type, blocks: pieces})
  //console.log(newBlock)
  return newBlock

}

export { rotateBlock, getNewBlock }