
let keyboard = (keyCode) => {
  let key = {};

  key.code = keyCode;
  key.isDown = false;
  key.isUp = true;
  key.press = undefined;
  key.release = undefined;

  //The `downHandler`
  key.downHandler = (event) => {
    if (event.keyCode === key.code) {
      if (key.isUp && key.press) key.press();
      key.isDown = true;
      key.isUp = false;
    }
    event.preventDefault();
  };

  //The `upHandler`
  key.upHandler = (event) => {
    if (event.keyCode === key.code) {
      if (key.isDown && key.release) key.release();
      key.isDown = false;
      key.isUp = true;
    }
    event.preventDefault();
  };

  //Attach event listeners
  window.addEventListener(
    'keydown', key.downHandler.bind(key), false
  );
  window.addEventListener(
    'keyup', key.upHandler.bind(key), false
  );
  return key;
}

//Capture the keyboard arrow keys
const
  left = keyboard(37),
  up = keyboard(38),
  right = keyboard(39),
  down = keyboard(40);

  const setupKeyboardCallbacks = (keyboardCallbacks) => {

    left.press = () => {
      keyboardCallbacks.onLeftPress.call()
    }

    left.release = () => {
      if (!right.isDown) {
        keyboardCallbacks.onLeftRelease.call()
      }
    }

    right.press = () => {
      keyboardCallbacks.onRightPress.call()
    }

    right.release = () => {
      if (!left.isDown) {
        keyboardCallbacks.onRightRelease.call()
      }
    }

    down.press = () => {
      keyboardCallbacks.onDownPress.call()
    }

    down.release = () => {
      keyboardCallbacks.onDownRelease.call()
    }

    up.press = () => {
      keyboardCallbacks.onUpPress.call()
    }

  }

export {setupKeyboardCallbacks}