import { addBlockToBoard, getCollisionsForBlocksInModel } from './physics'
import { clearFilledRows } from './board-handler'
import { rotateBlock , getNewBlock} from './blocks'

let blockModel = {}

let board = [
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]

const keyboardCallbacks = {
  onLeftPress: () => {
    blockModel.vx = -16
  },
  onLeftRelease: () => {
    blockModel.vx = 0
  },
  onRightPress: () => {
    blockModel.vx = 16
  },
  onRightRelease: () => {
    blockModel.vx = 0
  },
  onDownPress: () => {
    blockModel.vy = 8
  },
  onDownRelease: () => {
    blockModel.vy = 1
  },
  onUpPress: () => {

    let rotatedBlockModel = rotateBlock(blockModel)
    const collisions = getCollisionsForBlocksInModel(board, rotatedBlockModel)
    if (!collisions.some((c) => c.yCollision && c.xCollision)){
      //console.log(collisions)

      blockModel = rotatedBlockModel
    }
    // blockModel.blocks = [{x: 1, y: 0}, {x: 1, y: 1}, {x: 1, y: 2}, {x: 1, y: 3}]
    // get new model from blocks with rotation
    // check for collision -> if ok use the new model
  }
}

const createNewBlock = () => {
  blockModel = getNewBlock()
  return getBlockModelCurrentState()
}

const checkCollisionOnBlock = (handleYCollisionCallback) => {

  const collisions = getCollisionsForBlocksInModel(board, blockModel)

  if (collisions.some((c) => c.yCollision)) {

    //blockModel.y = snapToNextYTile(blockModel) // todo fix bouncy
    board = addBlockToBoard(board, blockModel)
    board = clearFilledRows(board)

    handleYCollisionCallback.call()

  } else if (collisions.some((c) => c.xCollision)) {
    blockModel.y += blockModel.vy
    blockModel.vx = 0
  } else {
    blockModel.y += blockModel.vy
    blockModel.x += blockModel.vx
    blockModel.vx = 0
  }

}

const getBlockModelCurrentState = () => {
  return Object.assign(blockModel) // todo check if it passes a deep copy instead of ref
}

const getBoardCurrentState = () => {
  return Object.assign(board) // todo check if it passes a deep copy instead of ref
}

export { keyboardCallbacks, createNewBlock, checkCollisionOnBlock, getBlockModelCurrentState, getBoardCurrentState }